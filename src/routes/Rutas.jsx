import React from 'react'
import { Route, Routes } from 'react-router'
import For0for from '../pages/404/For0for'
import Home from '../pages/home/Home'
import { BrowserRouter } from 'react-router-dom'
export default function Rutas() {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Home />} exact />
                    <Route path="*" element={<For0for />} />
                </Routes>
            </BrowserRouter>
        </>
    )
}


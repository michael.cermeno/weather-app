
import './App.css';
import Rutas from './routes/Rutas';

import Provider from './store/Provider';

function App() {
  return (
    <div>
      <Provider>
        <Rutas />
      </Provider>
    </div>
  );
}

export default App;

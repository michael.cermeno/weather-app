import React, { useContext } from 'react'


import './home.css'
import { useState } from 'react'
import ContextStore from '../../store/ContextStore'
export default function Home() {
    const { location, temperature, getdata, setChange, change } = useContext(ContextStore)
    const [loading, setLoading] = useState(false)
    ////////////////form
    function submit(e) {
        e.preventDefault()
        setLoading(true)
        try {
            getdata()
        } catch (err) {
            console.log(err)
        }
    }
    ////////////Input
    function handleChange(e) {
        setLoading(false)
        setChange(e.target.value)
    }
    ///////////click reset
    function handleClick() {
        setLoading(false)
        setChange("")
    }
    /////////////
    return (
        <div className="box-global">
            <div className={loading ? "box-search" : "box-date"}>
                <div>
                    <div className="title">Aplicacion del clima</div>
                    <form className="form" onSubmit={submit}>
                        <label className=''>Buscar Clima por Pais o Ciudad</label>
                        <br />
                        <input  type="search" onChange={handleChange} value={change} />
                        <button disabled={!change.length} >Buscar</button>
                        {loading && <button type='reset' onClick={handleClick} >Cerrar</button>}
                    </form>
                    {loading && temperature && location &&
                        <div className="result">
                            <ul>
                                <li><strong>Nombre:</strong> {location.name}
                                </li>
                                <li><strong>Region:</strong>{location.region}
                                </li>
                                <li><strong>Pais</strong>: {location.country}
                                </li>
                                <li><strong>Localizacion</strong>: {location.tz_id}
                                </li>
                                <li><strong>Fecha y Hora:</strong>{location.localtime}
                                </li>
                                <li><strong>Temperaura:</strong>  {temperature.temp_c}°C
                                </li>
                                <li><strong>Humedad:</strong> {temperature.humidity}%
                                </li>
                            </ul>
                        </div>}
                    {loading && !temperature && !location &&
                        <p>No se Encontraron Resultados de: <br /> <strong>{change}</strong></p>
                    }
                </div>
            </div>
        </div>
    )
}

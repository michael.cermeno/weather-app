import { createContext } from "react";

const ContextStore = createContext("Clima")

export default ContextStore;
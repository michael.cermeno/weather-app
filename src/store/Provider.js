import React, { useState } from 'react'
import ContextStore from './ContextStore'
import apiCall from '../components/api/Apicall'
function Provider({children}) {
    const [location, setLocation] = useState({})
    const [temperature, setTemperature] = useState({})
    const key = "46665881628842b19d2212727210912"
    const [change, setChange] = useState("")
    const URL  = `http://api.weatherapi.com/v1/current.json?key=${key}&q=${change}`

    async function getdata(){
        try{
            const fetchApi = await apiCall({url:URL})     
            setLocation(fetchApi.location)
            setTemperature(fetchApi.current)
            console.log(fetchApi)
        }catch(err){
            console.log(err)
            setLocation({});
            setTemperature({});
        }
        
    }
    
    return (
    <ContextStore.Provider value={{location,setLocation,temperature,setTemperature,setChange,getdata,change}}>
        {children}
    </ContextStore.Provider>
    )
}

export default Provider
